import 'package:flutter/material.dart';
import '../models/meal.dart';
import '../widgets/meal_item.dart';
//import '../dummy_data.dart';

class CategoryMealsPage extends StatefulWidget {
  static const routeName = '/category-meals';
  final List<Meal> setMeals;

  CategoryMealsPage(this.setMeals);

  @override
  _CategoryMealsPageState createState() => _CategoryMealsPageState();
}

class _CategoryMealsPageState extends State<CategoryMealsPage> {
  String categoryTitle;
  List<Meal> displayedMeals;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    //ModalRoute.of(context) cannot be used in initState()
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    final routeArgs =
        ModalRoute.of(context).settings.arguments as Map<String, String>;
    categoryTitle = routeArgs['title'];
    final categoryID = routeArgs['id'];
    displayedMeals = widget.setMeals.where((meal) {
      return meal.categories.contains(categoryID);
    }).toList();
  }

  void _removeMeal(String mealID) {
    setState(() {
      displayedMeals.removeWhere((meal) => meal.id == mealID);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(categoryTitle),
      ),
      body: ListView.builder(
        itemBuilder: (ctx, i) {
          return MealItem(
            id: displayedMeals[i].id,
            title: displayedMeals[i].title,
            imageUrl: displayedMeals[i].imageUrl,
            duration: displayedMeals[i].duration,
            affordability: displayedMeals[i].affordability,
            complexity: displayedMeals[i].complexity,
          );
        },
        itemCount: displayedMeals.length,
      ),
    );
  }
}
