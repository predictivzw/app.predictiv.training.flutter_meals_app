import 'package:flutter/material.dart';
import '../widgets/main_drawer.dart';

class FiltersPage extends StatefulWidget {
  static const routeName = '/filters-page';
  final Function saveFilters;
  final Map<String, bool> globalFilters;

  FiltersPage(this.globalFilters, this.saveFilters);

  @override
  _FiltersPageState createState() => _FiltersPageState();
}

class _FiltersPageState extends State<FiltersPage> {
  var _glutenFree = false;
  var _vegetarian = false;
  var _vegan = false;
  var _lactoseFree = false;

  void initState() {
    super.initState();
    _glutenFree = widget.globalFilters['gluten'];
    _vegetarian = widget.globalFilters['vegetarian'];
    _vegan = widget.globalFilters['vegan'];
    _lactoseFree = widget.globalFilters['lactose'];
  }

  Widget _buildSwitchListTile({
    @required String title,
    @required String subTitle,
    @required bool setVal,
    @required Function onUpdated,
  }) {
    return SwitchListTile(
      title: Text(
        title,
      ),
      value: setVal,
      subtitle: Text(subTitle),
      onChanged: onUpdated,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Filters'),
        actions: [
          IconButton(
            icon: Icon(Icons.save),
            onPressed: () {
              final selectedFilters = {
                'gluten': _glutenFree,
                'lactose': _lactoseFree,
                'vegan': _vegan,
                'vegetarian': _vegetarian,
              };

              widget.saveFilters(selectedFilters);
            },
          )
        ],
      ),
      drawer: MainDrawer(),
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.all(20),
            child: Text(
              'Adjust your meal selection.',
              style: Theme.of(context).textTheme.headline6,
            ),
          ),
          Expanded(
            child: ListView(
              children: [
                _buildSwitchListTile(
                  title: 'Gluten Free',
                  subTitle: 'Only gluten-free meals.',
                  setVal: _glutenFree,
                  onUpdated: (val) {
                    setState(() {
                      _glutenFree = val;
                    });
                  },
                ),
                _buildSwitchListTile(
                  title: 'Lactose Free',
                  subTitle: 'Only lactose-free meals',
                  setVal: _lactoseFree,
                  onUpdated: (val) {
                    setState(() {
                      _lactoseFree = val;
                    });
                  },
                ),
                _buildSwitchListTile(
                  title: 'Vegetarian',
                  subTitle: 'Only vegetarian meals',
                  setVal: _vegetarian,
                  onUpdated: (val) {
                    setState(() {
                      _vegetarian = val;
                    });
                  },
                ),
                _buildSwitchListTile(
                  title: 'Vegan',
                  subTitle: 'Only vegan meals',
                  setVal: _vegan,
                  onUpdated: (val) {
                    setState(() {
                      _vegan = val;
                    });
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
